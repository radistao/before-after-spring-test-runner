package org.bitbucket.radistao.test.runnerExample.dataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test example with JPA data source. Based on dependency you activate in {@code build.gradle}
 * you could run the test over in-memory database or over some local hosted Postgresql DB.
 * <p>
 * The main idea: before all the tests you could create and pre-populate a table, and then run
 * all the tests over these data.
 * <p>
 * See {@link #setupBeforeAll()} and {@link #tearDownAfterAll()} for more details.
 */
@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest
public class DataSourceApplicationIntegrationTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Create and populate {@code customers} table before all the test cases.
     * <p>
     * <b>Note: {@link #jdbcTemplate} won't be available in standard <code>@BeforeClass</code></b>
     */
    @BeforeAllMethods
    public void setupBeforeAll() {
        jdbcTemplate.execute("DROP TABLE IF EXISTS customers");
        jdbcTemplate.execute("CREATE TABLE customers(" +
                "id SERIAL PRIMARY KEY, first_name VARCHAR(255), last_name VARCHAR(255))");

        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("John Woo", "Jeff Dean", "Josh Bloch", "Josh Long").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        jdbcTemplate.batchUpdate("INSERT INTO customers(first_name, last_name) VALUES (?,?)", splitUpNames);
    }

    /**
     * Drop the database table after all the tests.
     * <p>
     * <b>Note: {@link #jdbcTemplate} won't be available in standard <code>@BeforeClass</code></b>
     */
    @AfterAllMethods
    public void tearDownAfterAll() {
        jdbcTemplate.execute("DROP TABLE IF EXISTS customers");
    }

    @Test
    public void joshBlochExists() throws Exception {
        Customer customer = getCustomer("Josh", "Bloch");
        assertThat(customer).isNotNull();
        assertThat(customer.getFirstName()).isEqualTo("Josh");
        assertThat(customer.getLastName()).isEqualTo("Bloch");
    }

    @Test
    public void johnWooExists() throws Exception {
        Customer customer = getCustomer("John", "Woo");
        assertThat(customer).isNotNull();
        assertThat(customer.getFirstName()).isEqualTo("John");
        assertThat(customer.getLastName()).isEqualTo("Woo");
    }

    @Test
    public void jeffDeanExists() throws Exception {
        Customer customer = getCustomer("Jeff", "Dean");
        assertThat(customer).isNotNull();
        assertThat(customer.getFirstName()).isEqualTo("Jeff");
        assertThat(customer.getLastName()).isEqualTo("Dean");
    }

    private Customer getCustomer(String firstName, String secondName) {
        return jdbcTemplate.query(
                "SELECT id, first_name, last_name FROM customers WHERE first_name = ? AND last_name = ?",
                new Object[]{firstName, secondName},
                (rs, rowNum) -> new Customer(rs.getLong("id"), rs.getString("first_name"),
                        rs.getString("last_name"))).get(0);
    }
}