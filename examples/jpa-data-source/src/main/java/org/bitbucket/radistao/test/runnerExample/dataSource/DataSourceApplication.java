package org.bitbucket.radistao.test.runnerExample.dataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Just a mock application. The main feature review is in the tests.
 */
@SpringBootApplication
public class DataSourceApplication {

    public static void main(String args[]) {
        SpringApplication.run(DataSourceApplication.class, args);
    }

}