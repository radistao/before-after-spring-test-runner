package org.bitbucket.radistao.test.runnerExample.slowDataSource;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

/**
 * Simulate some data source which has pre-filled data and doesn't have a fast way
 * to cleanup the repository.
 */
public class PaymentDataSourceImpl implements PaymentDataSource {

    // use static storage so all the tests in fact access the same instance.
    private static final Map<String, Payment> STORAGE = new HashMap<>();

    static {
        // pre-filled data
        STORAGE.put("Satoshi Nakamoto", new Payment("Satoshi Nakamoto", new BigDecimal(100500)));
        STORAGE.put("me", new Payment("me", new BigDecimal(0)));
    }

    @Override
    public Payment create(String payerId, BigDecimal amount) throws IllegalStateException {
        if (STORAGE.containsKey(payerId)) {
            throw new IllegalStateException(format("Payment from %s already exists", payerId));
        }
        Payment payment = new Payment(payerId, amount);
        STORAGE.put(payment.getPayerId(), payment);
        return payment;
    }

    @Override
    public Payment remove(String paymentId) {
        simulateSlowOperation();
        return STORAGE.remove(paymentId);
    }

    @Override
    public int count() {
        return STORAGE.size();
    }

    @Override
    public List<Payment> findAll() {
        simulateSlowOperation();
        return STORAGE.entrySet().stream()
                .map(Map.Entry::getValue)
                .collect(toList());
    }

    private static void simulateSlowOperation() {
        try {
            Thread.sleep(2500);
        } catch (InterruptedException ignore) {
        }
    }
}
