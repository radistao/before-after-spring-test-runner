package org.bitbucket.radistao.test.runnerExample.slowDataSource;

import java.math.BigDecimal;

public class Payment {
    private String payerId;
    private BigDecimal amount;

    public Payment(String payerId, BigDecimal amount) {
        this.payerId = payerId;
        this.amount = amount;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
