package org.bitbucket.radistao.test.runnerExample.slowDataSource;

import java.math.BigDecimal;
import java.util.List;

/**
 * Some data source access API. {@code payerId} expected to be unique value across the repository.
 */
public interface PaymentDataSource {
    /**
     * @throws IllegalStateException if the payerId key already exists.
     */
    Payment create(String payerId, BigDecimal amount) throws IllegalStateException;

    /**
     * A slow operation
     */
    Payment remove(String paymentId);

    int count();

    /**
     * A slow operation
     */
    List<Payment> findAll();
}
