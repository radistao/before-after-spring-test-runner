package org.bitbucket.radistao.test.runnerExample.slowDataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Just a mock application. The main feature review is in the tests.
 */
@SpringBootApplication
public class SlowDataSourceApplicationExample {

    public static void main(String args[]) {
        SpringApplication.run(SlowDataSourceApplicationExample.class, args);
    }

    @Bean
    public PaymentDataSource paymentDataSource() {
        return new PaymentDataSourceImpl();
    }
}