package org.bitbucket.radistao.test.runnerExample.slowDataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static java.lang.System.currentTimeMillis;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Example of how to use {@link BeforeAfterSpringTestRunner}, {@link BeforeAllMethods} and
 * {@link AfterAllMethods}.
 * <p>
 * In this example we simulate usage some test data source (not a DB), which has a limited API
 * and quite slow operations of fetching all and deleting.
 * <p>
 * The main idea: before all the tests you could cleanup the integration test resource, and then run
 * all the tests on clean source.
 * <p>
 * See {@link #setupBeforeAll()} and {@link #tearDownAfterAll()} for more details.
 */
@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest
public class SlowDataSourceApplicationExampleIntegrationTest {


    @Autowired
    private PaymentDataSource paymentDataSource;

    @Autowired
    private Logger mainLogger;

    /**
     * Clean the data source before the tests.
     * <p>
     * <b>Note: {@link #paymentDataSource} won't be available in standard <code>@BeforeClass</code></b>
     */
    @BeforeAllMethods
    public void setupBeforeAll() {

        assertThat(paymentDataSource.count()).isEqualTo(2);

        final long start = currentTimeMillis();

        mainLogger.info("Before Tests: start PaymentDataSource cleaning up...");

        // this is a slow operation, so we don't want to perform it on every @Before/@After
        paymentDataSource.findAll().stream()
                .map(Payment::getPayerId)
                .forEach(paymentDataSource::remove);

        mainLogger.info("Before Tests: PaymentDataSource cleaned up in {} msec", currentTimeMillis() - start);

        assertThat(paymentDataSource.count()).isEqualTo(0);
    }

    /**
     * Drop the data from the slow data source.
     * <p>
     * <b>Note: {@link #paymentDataSource} won't be available in standard <code>@BeforeClass</code></b>
     */
    @AfterAllMethods
    public void tearDownAfterAll() {
        assertThat(paymentDataSource.count()).isEqualTo(7);

        final long start = currentTimeMillis();

        mainLogger.info("After Tests: start PaymentDataSource cleaning up...");

        // this is a slow operation, so we don't want to perform it on every @Before/@After
        paymentDataSource.findAll().stream()
                .map(Payment::getPayerId)
                .forEach(paymentDataSource::remove);

        mainLogger.info("After Tests: PaymentDataSource cleaned up in {} msec", currentTimeMillis() - start);

        assertThat(paymentDataSource.count()).isEqualTo(0);
    }

    /**
     * Verify exception is not throws when we put the same values, as in initial
     * payment data source state, because they should be cleaned up in {@code {@link #setupBeforeAll()}}
     */
    @Test
    public void testInitialValuesCouldBeCreatedAgail() {
        paymentDataSource.create("Satoshi Nakamoto", new BigDecimal(100500));
        paymentDataSource.create("me", new BigDecimal(0));
    }

    @Test
    public void test2Entries() throws Exception {
        final int count = paymentDataSource.count();
        IntStream.range(0, 2)
                .forEach(i -> paymentDataSource.create("Payer2" + i, new BigDecimal(i * 20)));
        assertThat(paymentDataSource.count()).isEqualTo(count + 2);
    }

    @Test
    public void test3Entries() throws Exception {
        final int count = paymentDataSource.count();
        IntStream.range(0, 3)
                .forEach(i -> paymentDataSource.create("Payer3" + i, new BigDecimal(i * 30)));
        assertThat(paymentDataSource.count()).isEqualTo(count + 3);
    }

    @TestConfiguration
    public static class LoggerTestConfiguration {
        @Bean
        public Logger mainLogger() {
            return LoggerFactory.getLogger("TEST LOGGER");
        }
    }
}