# Before After Spring Test Runner

[ ![Download](https://api.bintray.com/packages/radistao/radistao-maven/before-after-spring-test-runner/images/download.svg?version=0.1.0) ](https://bintray.com/radistao/radistao-maven/before-after-spring-test-runner/0.1.0/link)

Java Spring test runner to execute dedicated ***non static*** methods
before the main test cases and after all of them.

This could be useful for _integration tests_, where DB or another data
repository (like some REST API) should be whipped out or pre-populated
before/after tests, but using default `@BeforeClass`/`@AfterClass`
is not convenient because _Spring_ context can't be injected
in these methods, hence _`dataSource`_ is unavailable.

- [Why we need it](#markdown-header-why-we-need-it)
- [Where to use](#markdown-header-where-to-use)
- [How to use](#markdown-header-how-to-use)
    - [Adding project dependency](#markdown-header-adding-project-dependency)
        - [Maven](#markdown-header-maven)
        - [Gradle](#markdown-header-gradle)
- [Difference between `BeforeAfterSpringTestRunner` and `BeforeAfterSpringTestRunner2`](#markdown-header-difference-between-beforeafterspringtestrunner-and-beforeafterspringtestrunner2)
- [Examples](#markdown-header-examples)
  - [jpa-data-source](#markdown-header-jpa-data-source)
  - [slow-data-source](#markdown-header-slow-data-source)
- [How to build](#markdown-header-how-to-build)

## Why we need it

The following Stackoverflow questions describe the problem:

  - [Junit - run set up method once](https://stackoverflow.com/questions/12087959)
  - [JUnit @BeforeClass non-static work around for Spring Boot application](https://stackoverflow.com/questions/32952884/)
  - [Junit before class ( non static )](https://stackoverflow.com/questions/2825615/)

Instead of using those hacks every time it was decided to implement a special annotation, 
which allows to solve the problem painless.

## Where to use

I'd say _unit testing_ likely a wrong place to use this approach, where
mocking should be sufficient enough.
But as mentioned above - some complex _integration test_ could
become easier if you prepare some staff before the tests.

## How to use

The approach is consist of 2 parts: `@BeforeAllMethods/@AfterAllMethods`
annotations on some test methods and
`@RunWith(BeforeAfterSpringTestRunner.class)`.

Example:

```java
@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest
public class SlowDataSourceApplicationExampleIntegrationTest {

    @Autowired
    private PaymentDataSource paymentDataSource;

    @Autowired
    private Logger mainLogger;

    /**
     * Clean the data source before the tests.
     * <p>
     * <b>Note: {@link #paymentDataSource} won't be available in standard <code>@BeforeClass</code></b>
     */
    @BeforeAllMethods
    public void setupBeforeAll() {
        mainLogger.info("Before Tests: start PaymentDataSource cleaning up...");
        // this is a slow operation, so we don't want to perform it on every @Before/@After
        paymentDataSource.removeAll();
    }

    /**
     * Drop the data from the slow data source.
     * <p>
     * <b>Note: {@link #paymentDataSource} won't be available in standard <code>@BeforeClass</code></b>
     */
    @AfterAllMethods
    public void tearDownAfterAll() {
        mainLogger.info("After Tests: start PaymentDataSource cleaning up...");
        // this is a slow operation, so we don't want to perform it on every @Before/@After
        paymentDataSource.removeAll();
    }

```

### Adding project dependency

#### Maven
    <dependency>
        <groupId>org.bitbucket.radistao.test</groupId>
        <artifactId>before-after-spring-test-runner</artifactId>
        <version>0.1.0</version>
        <scope>test</scope>
    </dependency>


#### Gradle

    testCompile group: 'org.bitbucket.radistao.test', name: 'before-after-spring-test-runner', version: '0.1.0'

## Difference between `BeforeAfterSpringTestRunner` and `BeforeAfterSpringTestRunner2`

`BeforeAfterSpringTestRunner2` (sorry, i was not been able to invent
better name) has almost the same behavior, with the only one difference:
in this case every method annotated with `@BeforeAllMethods/@AfterAllMethods`
will be preceded by `@Before` (aka _setUp()_) before execution and
finalized by `@After` (aka _tearDown()_) after one. So, the methods
behave the same way as normal `@Test`, except they have predicted order.

## Examples

Look at [`examples`](/examples) directory:

### [`jpa-data-source`](/examples/jpa-data-source)

Example of an integration test where `dataSource` is injected and a test
DB table is created and prepopulated before the test cases.
See [`DataSourceApplicationIntegrationTest`](/examples/jpa-data-source/src/test/java/org/bitbucket/radistao/test/runnerExample/dataSource/DataSourceApplicationIntegrationTest.java)
for more details.

### [`slow-data-source`](/examples/slow-data-source)

Example of an integration test where data is located not in a database,
but in some slow source which is used over some API interface.
Some times some REST APIs are not designed for fast cleaning,
thus you are doomed to fetch all the values an remove them one by one,
thus this could be very slow and one doesn't want to do the on every
before-after.
See [`SlowDataSourceApplicationExampleIntegrationTest`](/examples/slow-data-source/src/test/java/org/bitbucket/radistao/test/runnerExample/slowDataSource/SlowDataSourceApplicationExampleIntegrationTest.java)
for more details.

## How to build

See [`before-after-spring-test-runner/README.md`](/before-after-spring-test-runner-sources/README.md)
