#!/bin/sh

# the build version must not end with -SNAPSHOT
./gradlew clean build signArchives bintrayUpload "$@"
