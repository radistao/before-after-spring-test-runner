package org.bitbucket.radistao.test.runner.model;

/**
 * Same as {@link java.util.function.Supplier}, but expects exception in {@link #get()} call.
 *
 * @param <T> type of the generated value.
 */
@FunctionalInterface
public interface SupplierWithException<T> {

    T get() throws Exception;
}
