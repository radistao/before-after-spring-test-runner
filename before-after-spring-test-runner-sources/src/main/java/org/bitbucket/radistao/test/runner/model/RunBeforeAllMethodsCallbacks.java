package org.bitbucket.radistao.test.runner.model;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.List;

/**
 * This statement will run all the (test) methods form {@code allMethodsBefores}
 * over instances from {@code testInstanceSupplier} and after that will evaluate {@code next} statement.
 * <p>
 * New test class instance will be created for every single method from {@code allMethodsBefores}.
 *
 * @see RunAfterAllMethodsCallbacks
 */
public class RunBeforeAllMethodsCallbacks extends Statement {
    private final Statement beforeAllMethods;
    private final Statement next;

    public RunBeforeAllMethodsCallbacks(List<FrameworkMethod> allMethodsBefores,
                                        SupplierWithException<Object> testInstanceSupplier,
                                        Statement next) {
        this.next = next;
        this.beforeAllMethods = new BeforeAfterRunnerStatement(allMethodsBefores, testInstanceSupplier);
    }

    @Override
    public void evaluate() throws Throwable {
        this.beforeAllMethods.evaluate();
        this.next.evaluate();
    }
}
