package org.bitbucket.radistao.test.runner;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.model.RunAfterAllMethodsCallbacks;
import org.bitbucket.radistao.test.runner.model.RunBeforeAllMethodsCallbacks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * This class should be used as a test runner
 * (<code>@RunWith(BeforeAfterSpringTestRunner.class)</code>) together with {@link BeforeAllMethods}
 * and {@link AfterAllMethods} annotations.
 * <p>
 * The runner provides additional preparation steps before all test cases and after all of them.
 * This is very similar to {@link org.junit.BeforeClass} and {@link org.junit.AfterClass}
 * methods <b><i>except</i></b> they are not static an thus these method have an access to
 * Spring context! This is the main benefit of the methods: <b>you could cleanup/pre-fill a data source
 * (DB, REST service, etc)</b> before running all the test cases against this data source.
 * <p>
 * Opposite to {@link org.junit.Before} and {@link org.junit.After} methods these methods are run only once
 * on a test class.
 * <p>
 * The annotated methods should not be static.
 * <p>
 * Multiply methods could be annotated by {@link BeforeAllMethods} and {@link AfterAllMethods},
 * and all of them will be executed before/after the <code>@Test</code> cases respectively,
 * although the order in which the methods with the same annotation are executed <b>is undefined</b>.
 * <p>
 * The order in which the test methods are run is the next:
 * <ol>
 * <li><code>@BeforeClass</code> in the static context</li>
 * <li><code>@BeforeAllMethods</code> on a dedicated instance</li>
 * <li><code>@Before</code> on a new instance</li>
 * <li><code>@Test</code> on the same instance as <code>@Before</code></li>
 * <li><code>@After</code> on the same instance as <code>@Before</code> and <code>@Test</code></li>
 * <li><code>@AfterAllMethods</code> on a dedicated instance</li>
 * <li><code>@AfterClass</code> in the static context</li>
 * </ol>
 * <p>
 * <b>Example</b>: see <code>BeforeAfterSpringTestRunnerSequenceTest</code> and
 * <code>BeforeAfterSpringTestRunnerMultipleMethodsTest</code> in <code>/src/test/java/</code>as an example
 * how to apply the runner, annotations and use the injection in the BeforeAll/AfterAll methods.
 *
 * @see BeforeAfterSpringTestRunner2
 */
public class BeforeAfterSpringTestRunner extends SpringJUnit4ClassRunner {

    public BeforeAfterSpringTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    /**
     * Take the parent {@link Statement} and wrap it with before/after test method invokers,
     * if such annotated methods exist.
     *
     * @param notifier tests workflow notifier
     * @return new statement instance which runs the default tests in between {@link BeforeAllMethods}
     * and {@link AfterAllMethods} methods.
     */
    @Override
    protected Statement childrenInvoker(RunNotifier notifier) {
        Statement mainStatement = super.childrenInvoker(notifier);

        List<FrameworkMethod> allMethodsBefores = getTestClass()
                .getAnnotatedMethods(BeforeAllMethods.class);

        List<FrameworkMethod> allMethodsAfters = getTestClass()
                .getAnnotatedMethods(AfterAllMethods.class);

        RunBeforeAllMethodsCallbacks beforesStatement =
                new RunBeforeAllMethodsCallbacks(allMethodsBefores, this::createTest, mainStatement);

        RunAfterAllMethodsCallbacks aftersStatement =
                new RunAfterAllMethodsCallbacks(beforesStatement, allMethodsAfters, this::createTest);

        return aftersStatement;
    }
}
