package org.bitbucket.radistao.test.runner.model;

import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.List;

/**
 * This statement will create a test instance using {@code testInstanceSupplier}
 * and invoke all methods from {@code methodsToRun} on this newly created instance.
 */
public class BeforeAfterRunnerStatement extends Statement {

    private final List<FrameworkMethod> methodsToRun;
    private final SupplierWithException<Object> testInstanceSupplier;

    public BeforeAfterRunnerStatement(List<FrameworkMethod> methodsToRun,
                                      SupplierWithException<Object> testInstanceSupplier) {
        this.methodsToRun = methodsToRun;
        this.testInstanceSupplier = testInstanceSupplier;
    }

    @Override
    public void evaluate() throws Throwable {
        Object test;
        test = new ReflectiveCallable() {
            @Override
            protected Object runReflectiveCall() throws Throwable {
                return testInstanceSupplier.get();
            }
        }.run();

        for (FrameworkMethod method : methodsToRun) {
            method.invokeExplosively(test);
        }
    }
}
