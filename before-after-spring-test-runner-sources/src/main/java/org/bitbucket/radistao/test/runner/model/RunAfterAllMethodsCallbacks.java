package org.bitbucket.radistao.test.runner.model;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.List;

/**
 * This statement will run {@code first} statement, and after that will call methods from {@code allMethodsAfters}
 * on instances provided by {@code testInstanceSupplier}.
 * <p>
 * New test class instance will be created for every single method from {@code allMethodsAfters}.
 *
 * @see RunBeforeAllMethodsCallbacks
 */
public class RunAfterAllMethodsCallbacks extends Statement {
    private final Statement first;
    private final Statement afterAllMethods;

    public RunAfterAllMethodsCallbacks(Statement first,
                                       List<FrameworkMethod> allMethodsAfters,
                                       SupplierWithException<Object> testInstanceSupplier) {
        this.first = first;
        this.afterAllMethods = new BeforeAfterRunnerStatement(allMethodsAfters, testInstanceSupplier);
    }

    @Override
    public void evaluate() throws Throwable {
        this.first.evaluate();
        this.afterAllMethods.evaluate();
    }
}
