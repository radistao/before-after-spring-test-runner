package org.bitbucket.radistao.test.runner;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Experimental</b>
 * <p>
 * Same as {@link BeforeAfterSpringTestRunner}, but runs also {@link org.junit.Before} and {@link org.junit.After}
 * test methods when {@link BeforeAllMethods} and {@link AfterAllMethods} are invoked,
 * so that <i>before/after all method</i> use the same <i>setUp</i> and <i>tearDown</i>
 * as regular {@link org.junit.Test} methods.
 * <p>
 * In this case the order in which the test methods are run is the next:
 * <ol>
 * <li><code>@BeforeClass</code> in the static context</li>
 * <li><code>@Before</code> on a new instance</li>
 * <li><code>@BeforeAllMethods</code> on the same instance as <code>@Before</code> above</li>
 * <li><code>@After</code> on the same instance as <code>@Before</code> and <code>@BeforeAllMethods</code> above</li>
 * <li><code>@Before</code> on a new instance</li>
 * <li><code>@Test</code> on the same instance as <code>@Before</code> <b>5</b></li>
 * <li><code>@After</code> on the same instance as <code>@Before</code> <b>5</b> and <code>@Test</code> <b>6</b></li>
 * <li><code>@Before</code> on a new instance</li>
 * <li><code>@AfterAllMethods</code> on the same instance as <code>@Before</code> <b>8</b></li>
 * <li><code>@After</code> on the same instance as <code>@Before</code> <b>8</b> and <code>@AfterAllMethods</code></li>
 * <li><code>@AfterClass</code> in the static context</li>
 * </ol>
 * <p>
 * BeforeAfterSpringTestRunner
 */
public class BeforeAfterSpringTestRunner2 extends SpringJUnit4ClassRunner {

    public BeforeAfterSpringTestRunner2(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        List<FrameworkMethod> regularTests = super.computeTestMethods();
        List<FrameworkMethod> beforeAllTests = getTestClass().getAnnotatedMethods(BeforeAllMethods.class);
        List<FrameworkMethod> afterAllTests = getTestClass().getAnnotatedMethods(AfterAllMethods.class);
        List<FrameworkMethod> result = new ArrayList<>(beforeAllTests.size() + regularTests.size() + afterAllTests.size());

        result.addAll(beforeAllTests);
        result.addAll(regularTests);
        result.addAll(afterAllTests);

        return result;
    }
}
