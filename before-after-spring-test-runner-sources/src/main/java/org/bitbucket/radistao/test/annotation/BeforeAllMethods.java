package org.bitbucket.radistao.test.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate non-static method in a test class to run it before any other {@link org.junit.Test} annotated
 * method.
 * <p>
 * Requires running with special runner.
 * <p>
 * See more info in {@link org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner} and
 * {@link org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner2}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface BeforeAllMethods {
}
