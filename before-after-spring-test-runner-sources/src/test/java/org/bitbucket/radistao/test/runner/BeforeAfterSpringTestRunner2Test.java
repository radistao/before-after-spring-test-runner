package org.bitbucket.radistao.test.runner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.bitbucket.radistao.test.config.TestClassForTestingRunner;

import java.util.List;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Simple test to verify {@link BeforeAfterSpringTestRunner2#computeTestMethods()} returns {@link FrameworkMethod}s list
 * in right order.
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class BeforeAfterSpringTestRunner2Test {

    private BeforeAfterSpringTestRunner2 beforeAfterRunner2;

    @Before
    public void setUp() throws Exception {
        beforeAfterRunner2 = new BeforeAfterSpringTestRunner2(TestClassForTestingRunner.class);
    }

    @Test
    public void computeTestMethods_returnsMethodsInCorrectOrder() throws Exception {
        List<FrameworkMethod> frameworkMethods = beforeAfterRunner2.computeTestMethods();
        assertThat(frameworkMethods.size(), is(6)); // 2 beforeAll + 2 test + 2 afterAll

        int beforeAllOneIndex = getMethodIndexByName(frameworkMethods, "beforeAllMethods_one");
        int beforeAllTwoIndex = getMethodIndexByName(frameworkMethods, "beforeAllMethods_two");
        int testOneIndex = getMethodIndexByName(frameworkMethods, "testMethod_one");
        int testTwoIndex = getMethodIndexByName(frameworkMethods, "testMethod_two");
        int afterAllOneIndex = getMethodIndexByName(frameworkMethods, "afterAllMethods_one");
        int afterAllTwoIndex = getMethodIndexByName(frameworkMethods, "afterAllMethods_two");

        // since there is no guarantee of in which order the methods with the same annotation are executed,
        // we validate only that two of beforeAll are the first two in the list,
        // normal tests - are the second pair in the list,
        // afterAll - are the last pair in the list

        assertThat(beforeAllOneIndex + beforeAllTwoIndex, is(0 + 1));
        assertThat(testOneIndex + testTwoIndex, is(2 + 3));
        assertThat(afterAllOneIndex + afterAllTwoIndex, is(4 + 5));
    }

    private static int getMethodIndexByName(List<FrameworkMethod> frameworkMethods, String methodName) {
        for (int i = 0; i < frameworkMethods.size(); i++) {
            FrameworkMethod method = frameworkMethods.get(i);
            if (method.getName().equals(methodName)) {
                return i;
            }
        }

        throw new AssertionError(format("Can't find [%s] method in the frameworkMethods list", methodName));
    }
}