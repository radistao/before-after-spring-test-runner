package org.bitbucket.radistao.test.runner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.Statement;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.bitbucket.radistao.test.config.TestClassForTestingChildrenInvoker;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Simple test to verify {@link Statement#evaluate()} is called when wrapped in
 * {@link BeforeAfterSpringTestRunner#withBeforeClasses(Statement)} and
 * {@link BeforeAfterSpringTestRunner#withAfterClasses(Statement)}
 */
@RunWith(MockitoJUnitRunner.class)
public class BeforeAfterSpringTestRunnerTest {

    @Spy
    private RunNotifier notifier = new RunNotifier();

    private BeforeAfterSpringTestRunner beforeAfterRunner;

    @Before
    public void setUp() throws Exception {
        beforeAfterRunner = new BeforeAfterSpringTestRunner(TestClassForTestingChildrenInvoker.class);
    }

    /**
     * Ensure the statement from overridden {@link BeforeAfterSpringTestRunner#childrenInvoker(RunNotifier)}
     * is still processed correctly when {@link Statement#evaluate()} is called.
     *
     * @throws Throwable if any
     */
    @Test
    public void childrenInvoker_processesNotifierCorrectly() throws Throwable {
        Statement statement = beforeAfterRunner.childrenInvoker(notifier);
        statement.evaluate();

        // expected 6 test cases in TestClassForTestingChildrenInvoker
        verify(notifier, times(6)).fireTestStarted(any(Description.class));
        verify(notifier, times(6)).fireTestFinished(any(Description.class));

        // 3 of them failed, 1 ignored, 1 has failed assumption
        verify(notifier, times(3)).fireTestFailure(any(Failure.class));
        verify(notifier, times(1)).fireTestIgnored(any(Description.class));
        verify(notifier, times(1)).fireTestAssumptionFailed(any());
    }

}