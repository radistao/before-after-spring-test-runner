package org.bitbucket.radistao.test.runner.model;

import org.junit.Test;
import org.junit.runners.model.FrameworkMethod;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class BeforeAfterRunnerStatementTest {

    /**
     * Ensure {@link BeforeAfterRunnerStatement#evaluate()} calls all methods from <i>methodsToRun</i> list,
     * but doesn't call any other methods from supplied test instance.
     *
     * @throws Throwable if any
     */
    @Test
    public void evaluateCallsMethodsFromTheList() throws Throwable {
        // get list of all methods from TestClassForMethodInvoker, except method "three"
        List<FrameworkMethod> methodsWithoutThree = stream(TestClassForMethodInvoker.class.getDeclaredMethods())
                .filter(method -> !"three".equals(method.getName()))
                .map(FrameworkMethod::new)
                .collect(toList());

        TestClassForMethodInvoker test = new TestClassForMethodInvoker();

        BeforeAfterRunnerStatement statement = new BeforeAfterRunnerStatement(methodsWithoutThree, () -> test);

        statement.evaluate();

        assertThat(test.calledMethods).containsExactlyInAnyOrder("one is called", "two is called");
        // ensure the evaluator calls method only from the list, and skips other methods
        assertThat(test.calledMethods).doesNotContain("three is called"); // "three" is filtered
    }

}