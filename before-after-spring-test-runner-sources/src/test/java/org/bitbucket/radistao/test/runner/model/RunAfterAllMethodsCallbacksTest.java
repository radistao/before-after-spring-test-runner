package org.bitbucket.radistao.test.runner.model;

import org.junit.Test;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class RunAfterAllMethodsCallbacksTest {

    @Test
    public void evaluatesAreCalledInExpectedOrder() throws Throwable {
        // get list of all methods from TestClassForMethodInvoker, except method "one"
        List<FrameworkMethod> methodsWithoutOne = stream(TestClassForMethodInvoker.class.getDeclaredMethods())
                .filter(method -> !"one".equals(method.getName()))
                .map(FrameworkMethod::new)
                .collect(toList());

        TestClassForMethodInvoker test = new TestClassForMethodInvoker();

        Statement first = new Statement() {
            @Override
            public void evaluate() throws Throwable {
                test.calledMethods.add("first#evaluate() is called");
            }
        };

        RunAfterAllMethodsCallbacks callbacks =
                new RunAfterAllMethodsCallbacks(first, methodsWithoutOne, () -> test);

        callbacks.evaluate();

        assertThat(test.calledMethods.size()).isEqualTo(3);
        assertThat(test.calledMethods.get(0)).isEqualTo("first#evaluate() is called");
        assertThat(test.calledMethods.subList(1, 3)).containsExactlyInAnyOrder("two is called", "three is called");
    }

}