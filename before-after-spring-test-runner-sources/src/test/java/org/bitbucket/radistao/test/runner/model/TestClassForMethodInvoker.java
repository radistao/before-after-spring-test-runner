package org.bitbucket.radistao.test.runner.model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to fetch and call over reflection the methods.
 * Then the called methods and the order of calling could be verified from {@link #calledMethods} list.
 */
public class TestClassForMethodInvoker {
    List<String> calledMethods = new ArrayList<>();

    public void one() {
        calledMethods.add("one is called");
    }

    public int two() {
        calledMethods.add("two is called");
        return 0;
    }

    public String three() {
        calledMethods.add("three is called");
        return "";
    }
}
