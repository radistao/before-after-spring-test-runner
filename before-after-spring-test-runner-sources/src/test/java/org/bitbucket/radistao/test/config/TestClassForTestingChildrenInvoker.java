package org.bitbucket.radistao.test.config;

import org.junit.*;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunnerTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;

/**
 * This class is used as input argument for {@link org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunnerTest}.
 * It should not be run as a normal test case in the project build workflow, thus it is ignored.
 * <p>
 * The assertion results in this class are matter, because in
 * {@link BeforeAfterSpringTestRunnerTest#childrenInvoker_processesNotifierCorrectly()} we count number of
 * run, successful, failed, ignored and so on tests.
 */
@Ignore("This class should be permanently ignored since it is used as a testing config.")
public class TestClassForTestingChildrenInvoker {

    @BeforeClass
    public static void beforeClassSuccess() {
        assertThat(true, is(true));
    }

    @BeforeClass
    public static void beforeClassFailed() {
        //assertThat(true, is(false));
    }

    @AfterClass
    public static void afterClassSuccess() {
        assertThat(true, is(true));
    }

    @AfterClass
    public static void afterClassFailed() {
        //assertThat(true, is(false));
    }

    @BeforeAllMethods
    public void beforeAllMethodsSuccess() {
        assertThat(true, is(true));
    }

    @BeforeAllMethods
    public void beforeAllMethodsFailed() {
        //assertThat(true, is(false));
    }

    @AfterAllMethods
    public void afterAllMethodsSuccess() {
        assertThat(true, is(true));
    }

    @AfterAllMethods
    public void afterAllMethodsFailed() {
        //assertThat(true, is(false));
    }

    @Before
    public void setUpSuccess() throws Exception {
        assertThat(true, is(true));
    }

    @Before
    public void setUpFailed() throws Exception {
        //assertThat(true, is(false));
    }

    @After
    public void tearDownSuccess() throws Exception {
        assertThat(true, is(true));
    }

    @After
    public void tearDownFailed() throws Exception {
        //assertThat(true, is(false));
    }

    @Test
    public void testMethodSuccess() {
        assertThat(true, is(true));
    }

    @Test
    public void testMethodSuccess2() {
        assertThat(true, is(true));
    }

    @Test
    public void testMethodFailed() {
        assertThat(true, is(false));
    }

    @Test
    public void testMethodFailed2() {
        assertThat(2 + 2, is(5));
    }

    @Test
    @Ignore
    public void ignoredTest() {
        assertThat("should be ignored", is("whatever"));
    }

    @Test
    public void assumptionFailed() {
        assumeThat(1, is(2));
    }

    @Test
    public void testWithException() throws Exception {
        throw new Exception("Test exception");
    }
}
