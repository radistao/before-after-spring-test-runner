package org.bitbucket.radistao.test.runner.model;

import org.junit.Test;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class RunBeforeAllMethodsCallbacksTest {

    @Test
    public void evaluatesAreCalledInExpectedOrder() throws Throwable {
        // get list of all methods from TestClassForMethodInvoker, except method "two"
        List<FrameworkMethod> methodsWithoutTwo = stream(TestClassForMethodInvoker.class.getDeclaredMethods())
                .filter(method -> !"two".equals(method.getName()))
                .map(FrameworkMethod::new)
                .collect(toList());

        TestClassForMethodInvoker test = new TestClassForMethodInvoker();

        Statement next = new Statement() {
            @Override
            public void evaluate() throws Throwable {
                test.calledMethods.add("next#evaluate() is called");
            }
        };

        RunBeforeAllMethodsCallbacks callbacks =
                new RunBeforeAllMethodsCallbacks(methodsWithoutTwo, () -> test, next);

        callbacks.evaluate();

        assertThat(test.calledMethods.size()).isEqualTo(3);
        assertThat(test.calledMethods.subList(0, 2)).containsExactlyInAnyOrder("one is called", "three is called");
        assertThat(test.calledMethods.get(2)).isEqualTo("next#evaluate() is called");
    }

}