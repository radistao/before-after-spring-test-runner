package org.bitbucket.radistao.test.config;

import org.springframework.context.annotation.Bean;

/**
 * Test configuration to inject into testing classes.
 * It is included to the tests explicitly using
 * {@link org.springframework.test.context.ContextConfiguration} annotation.
 */
public class BeforeAfterSpringTestRunnerTestConfig {

    @Bean
    public Integer intFromConfig() {
        return 48;
    }

    @Bean
    public String strFromConfig() {
        return "hello";
    }
}
