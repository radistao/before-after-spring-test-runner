package org.bitbucket.radistao.test.config;

import org.junit.*;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner2Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * This class is used as input argument for {@link BeforeAfterSpringTestRunner2Test}.
 * It should not be run as a normal test case in the project build workflow, thus it is ignored.
 */
@Ignore("This class should be permanently ignored since it is used as a testing config.")
public class TestClassForTestingRunner {

    @BeforeClass
    public static void beforeClass() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @AfterClass
    public static void afterClass() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @BeforeAllMethods
    public void beforeAllMethods_one() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @BeforeAllMethods
    public void beforeAllMethods_two() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @AfterAllMethods
    public void afterAllMethods_one() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @AfterAllMethods
    public void afterAllMethods_two() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @Before
    public void setUp() throws Exception {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @After
    public void tearDown() throws Exception {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @Test
    public void testMethod_one() {
        assertThat("Test is executed", is("Test should not be executed"));
    }

    @Test
    public void testMethod_two() {
        assertThat("Test is executed", is("Test should not be executed"));
    }
}
