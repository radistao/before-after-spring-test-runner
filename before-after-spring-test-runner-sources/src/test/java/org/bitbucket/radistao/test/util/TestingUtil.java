package org.bitbucket.radistao.test.util;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class TestingUtil {
    /**
     * Assert the static testing list {@code actualList} has expected values like in {@code reporterExpected}.
     *
     * @param expectedSize expectedList {@code reporter}'s size to validate. The rest of the elements will be skipped.
     */
    public static void assertListsElements(final int expectedSize, final List actualList, final List expectedList) {

        assertThat("Comparing the same array as actualList and expectedList value",
                actualList, is(not(sameInstance(expectedList))));

        assertThat("actual list expected to be non-null value", actualList, is(notNullValue()));
        assertThat("expected list expected to be non-null value", expectedList, is(notNullValue()));

        //assertThat(actualList.size(), is(expectedSize));
        assertThat(expectedSize, is(lessThanOrEqualTo(expectedList.size())));

        for (int i = 0; i < expectedSize; i++) {
            assertThat(actualList.get(i), is(expectedList.get(i)));
        }
    }

    /**
     * Find all items in the list equal to {@code value}
     *
     * @param list  list to search
     * @param value value to find and collect indexes
     * @param <T>   type of list elements
     * @return non-null list of indices of items which are equal to {@code value}
     * in order how they appear in the {@code list}.
     */
    public static <T> List<Integer> indexOfAll(final List<T> list, final T value) {
        if (list == null) {
            return emptyList();
        }

        return IntStream.range(0, list.size())
                .filter(index -> Objects.equals(value, list.get(index)))
                .boxed()
                .collect(Collectors.toList());
    }
}
