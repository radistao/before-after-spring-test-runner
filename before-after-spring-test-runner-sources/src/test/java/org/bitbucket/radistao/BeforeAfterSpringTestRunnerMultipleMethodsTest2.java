package org.bitbucket.radistao;

import org.junit.*;
import org.junit.runner.RunWith;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.config.BeforeAfterSpringTestRunnerTestConfig;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.bitbucket.radistao.test.util.TestingUtil.assertListsElements;
import static org.bitbucket.radistao.test.util.TestingUtil.indexOfAll;

/**
 * Test all of {@link BeforeAllMethods} and {@link AfterAllMethods} are called for {@link BeforeAfterSpringTestRunner2}
 */
@RunWith(BeforeAfterSpringTestRunner2.class)
@ContextConfiguration(classes = BeforeAfterSpringTestRunnerTestConfig.class)
@SpringBootTest(properties = "test.property.from.annotation2=valueYYY")
public class BeforeAfterSpringTestRunnerMultipleMethodsTest2 {

    private static final List<String> reporter = new ArrayList<>();
    private static final List<String> reporterExpected = Arrays.asList(
            "beforeClass method is called",
            "setUp method is called",
            "beforeAllMethods method is called",
            "tearDown method is called",
            "setUp method is called",
            "beforeAllMethods method is called",
            "tearDown method is called",
            "setUp method is called",
            "beforeAllMethods method is called",
            "tearDown method is called",
            "setUp method is called",
            "test method is called",
            "tearDown method is called",
            "setUp method is called",
            "test method is called",
            "tearDown method is called",
            "setUp method is called",
            "afterAllMethods method is called",
            "tearDown method is called",
            "setUp method is called",
            "afterAllMethods method is called",
            "tearDown method is called",
            "setUp method is called",
            "afterAllMethods method is called",
            "tearDown method is called",
            "afterClass method is called"
    );

    @Autowired
    @Qualifier("intFromConfig")
    private Integer intFromConfig;

    @Autowired
    @Qualifier("strFromConfig")
    private String strFromConfig;

    @Value(value = "${test.property.from.annotation2}")
    private String testPropertyFromAnnotation2;

    @Value(value = "${value.from.application.properties}")
    private String valueFromAppProperties;

    @Value(value = "${integer.value.from.application.properties}")
    private Integer integerValueFromAppProperties;

    @BeforeAllMethods
    public void beforeAllMethods1() {
        validateInjections();
        reporter.add("beforeAllMethods method is called");
    }

    @BeforeAllMethods
    public void beforeAllMethods2() {
        validateInjections();
        reporter.add("beforeAllMethods method is called");
    }

    @BeforeAllMethods
    public void beforeAllMethods3() {
        validateInjections();
        reporter.add("beforeAllMethods method is called");
    }

    @AfterAllMethods
    public void afterAllMethods1() {
        validateInjections();
        reporter.add("afterAllMethods method is called");
    }

    @AfterAllMethods
    public void afterAllMethods2() {
        validateInjections();
        reporter.add("afterAllMethods method is called");
    }

    @AfterAllMethods
    public void afterAllMethods3() {
        validateInjections();
        reporter.add("afterAllMethods method is called");
    }

    @BeforeClass
    public static void beforeClass() {
        reporter.add("beforeClass method is called");
        assertReporterList(1);
    }

    @AfterClass
    public static void afterClass() {
        reporter.add("afterClass method is called");
        assertReporterList(26);

        // final assert calling order
        assertThat(indexOfAll(reporter, "beforeClass method is called"), contains(0));
        assertThat(indexOfAll(reporter, "beforeAllMethods method is called"), contains(2, 5, 8));
        assertThat(indexOfAll(reporter, "setUp method is called"), contains(1, 4, 7, 10, 13, 16, 19, 22));
        assertThat(indexOfAll(reporter, "test method is called"), contains(11, 14));
        assertThat(indexOfAll(reporter, "tearDown method is called"), contains(3, 6, 9, 12, 15, 18, 21, 24));
        assertThat(indexOfAll(reporter, "afterAllMethods method is called"), contains(17, 20, 23));
        assertThat(indexOfAll(reporter, "afterClass method is called"), contains(25));
    }

    @Before
    public void setUp() {
        validateInjections();
        reporter.add("setUp method is called");
    }

    @After
    public void tearDown() throws Exception {
        validateInjections();
        reporter.add("tearDown method is called");
    }

    @Test
    public void test1() {
        validateInjections();
        reporter.add("test method is called");
    }

    @Test
    public void zTest() {
        validateInjections();
        reporter.add("test method is called");
    }

    /**
     * Validate default Spring injections still work.
     */
    private void validateInjections() {
        assertThat(intFromConfig, is(48));
        assertThat(strFromConfig, is("hello"));
        assertThat(testPropertyFromAnnotation2, is("valueYYY"));
        assertThat(valueFromAppProperties, is("Yes, i'm from application.properties"));
        assertThat(integerValueFromAppProperties, is(789));
    }

    /**
     * Assert the static testing list {@link #reporter} has expected values like in {@link #reporterExpected}.
     *
     * @param expectedSize expected {@code reporter}'s size to validate.
     */
    private static void assertReporterList(final int expectedSize) {
        assertListsElements(expectedSize, reporter, reporterExpected);
    }
}
