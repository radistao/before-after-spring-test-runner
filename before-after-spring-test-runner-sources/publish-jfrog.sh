#!/bin/sh

# the build version must end with -SNAPSHOT
./gradlew clean build signArchives artifactoryPublish "$@"
