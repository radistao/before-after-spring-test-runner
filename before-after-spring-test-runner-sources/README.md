## How to build and deploy

### Build

```bash
./gradlew clean build
```

is sufficient to build and test the app.

### Deploy
To upload you have 3 options:

- upload to Bintray (only not _SNAPSHOT_ version) - use [publish-bintray.sh](./publish-bintray.sh)

- upload to OSS Jfrog (only _SNAPSHOT_ version) - use [publish-jfrog.sh](./publish-jfrog.sh)

- upload to OSS Sonatype (both _SNAPSHOT_ and release) - use [publish-sonatype.sh](./publish-sonatype.sh)

All the builds by default will sign the archives in `:signArchives` task.

The credentials to the signing and remote repos access should be
specified in `gradle.properties` (local project one, or in `$HOME/.gradle`).

**NOTE**: if you use local project `gradle.properties` - it **MUST** be
excluded from version control.

Mandatory `gradle.properties` settings:

```
signing.keyId=[id of the key in the keyring]
signing.password=[passfrase]
signing.secretKeyRingFile=[location of the keyring file on local file system]

# only for sonatype task
ossrhUsername=[same as for https://oss.sonatype.org]
ossrhPassword=[API key]

# only for bintray and jfrog tasks
bintrayUser=[https://bintray.com/]
bintrayPassword=[API key]

```

Archives signing could be skipped explicitly:

```bash
./gradlew clean build -x signArchives
```