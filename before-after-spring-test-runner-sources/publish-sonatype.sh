#!/bin/sh

# if version ends with -SNAPSHOT - publish to snapshots,
# otherwise - publish to staging
./gradlew clean build signArchives uploadArchives "$@"
